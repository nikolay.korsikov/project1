# code for main login menu
from admin import admin_menu
from guest import guest_menu
from quit import exit_prog

print('\t\t***TESTING SYSTEM***')


def start_menu():
    choice = 0
    while choice != 3:
        print('\n\t\tMenu'
              '\n1. Administrator'
              '\n2. Guest'
              '\n3. Exit')
        choice = int(input('>>> '))

        match choice:
            case 1:
                admin_menu()
                pass
            case 2:
                guest_menu()
            case 3:
                exit_prog()


# invoking program
start_menu()
