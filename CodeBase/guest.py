# code for guest menu
from quit import exit_prog, go_on
import json


#  connect to db_results.txt and dump into temp memory results
try:
    db_file = open('db_results.txt', 'r')
    db_file_str = db_file.read()
    db_results = json.loads(db_file_str)  # this file contains dump of results when program started
    db_file.close()
except Exception as e:
    print(str(e))


#  connect to guestlist.txt and dump into temp memory users
try:
    file = open('guestlist.txt', 'r')
    file_str = file.read()
    guestlist = json.loads(file_str)
    file.close()
except Exception as e:
    print(str(e))


# declaring all variables, dictionaries and lists
details = ["login", "password", "name", "surname", "home address", "telephone"]
# {"login1":{"login":"login1", "password":"pass", "name":"testname", "surname":"testsurname", "home address":"address",
# "telephone":"111"}, "login2":{"login":"login2", "password":"pass", "name":"testname", "surname":"testsurname",
# "home address":"address", "telephone":"111"}}


def guest_menu():
    guest_choice = 0
    while guest_choice != 3:
        print('\n\t\tGuest menu'
              '\n1. New user'
              '\n2. Returning user'
              '\n3. Exit')
        guest_choice = int(input('>>> '))

        match guest_choice:
            case 1:
                new_user()
            case 2:
                returning_user()
            case 3:
                exit_prog()
    pass


def new_user():
    print('\n\t\tNew user menu:')
    record = {}
    for key in details:
        value = input(f'Enter {key} of the person: ')

        # testing if login is unique or not
        for i in guestlist:
            if value == (guestlist[i]['login']):
                print('There is already a user with such login.')
                new_user()
            else:
                continue

        record[key] = value
    print(f'\nRecord {record["login"]} added:')
    for key, value in record.items():
        print(f'{key}: {value}')

    # adding personal record (small dict) to guest-list dictionary (big dict)
    user = record["login"]
    guestlist[user] = record

    # save in temp current user login
    log_file = open('current_user_log.txt', 'w')
    log_file.write(user)
    log_file.close()

    # adding new user to database file
    save_guestlist()
    existing_user_menu()


def returning_user():
    print('\n\t\tReturning user menu:')
    ru_login = input('Login:')

    # validation if there is existing user with such login
    user_name = 0
    user_surname = 0

    for i in guestlist:
        if (guestlist[i]['login']) == ru_login:
            user_name = guestlist[i]['name']
            user_surname = guestlist[i]['surname']

    if user_name == 0:
        print('There is no user with such login.')
    else:
        print(f"\nUser with login [{ru_login}] is {user_name}. ")

    # validation if password is matching login
    ru_password = input('Password:')
    if ru_password == guestlist[ru_login]['password']:
        print(f'Hello {user_name} {user_surname}!')

        # save in temp current user login
        log_file = open('current_user_log.txt', 'w')
        log_file.write(ru_login)
        log_file.close()

        # take user to next menu
        existing_user_menu()

    else:
        print('Wrong password!')


def save_guestlist():
    try:
        guest_list_file = open('guestlist.txt', 'w')
        guestlist_str = json.dumps(guestlist)
        guest_list_file.write(guestlist_str)
        guest_list_file.close()

    except Exception as error:
        print(str(error))
    print('\n Records saved.')


def existing_user_menu():
    eu_choice = 0

    while eu_choice != 3:
        print('\n\t\tExisting user menu:'
              '\n1. See prior results'
              '\n2. Take a test'
              '\n3. Exit')
        eu_choice = int(input('>>>'))

        match eu_choice:
            case 1:
                see_results()
            case 2:
                tests()
            case 3:
                exit_prog()
    pass


# code for test menus


def tests():
    test_selected = 0

    while test_selected != 3:
        print('\n\t\tTests menu:'
              '\n1. Sport'
              '\n2. Programming'
              '\n3. Return to prior menu')
        test_selected = int(input('>>> '))

        match test_selected:
            case 1:
                sport_tests()
            case 2:
                programming_tests()
            case 3:
                existing_user_menu()
    pass


def sport_tests():
    sport_selected = 0

    while sport_selected != 3:
        print('\n\t\tSport tests menu:'
              '\n1. Running'
              '\n2. Bodybuilding'
              '\n3. Return to prior menu')
        sport_selected = int(input('>>> '))

        match sport_selected:
            case 1:
                running_test()
            case 2:
                bb_test()
            case 3:
                tests()
    pass


def programming_tests():
    pl_selected = 0

    while pl_selected != 3:
        print('\n\t\tProgramming tests menu:'
              '\n1. Java script'
              '\n2. Python'
              '\n3. Return to prior menu')
        pl_selected = int(input('>>> '))

        match pl_selected:
            case 1:
                js_test()
            case 2:
                python_test()
            case 3:
                tests()
    pass

# *** TESTS ***


def ext_user():
    # function to extract current user login
    file_u = open("current_user_log.txt", "r")
    login = file_u.read()
    file_u.close()
    return login


def save_results():
    # todo add code for saving temp results in dictionary in db_results.txt
    file2 = open('db_results.txt', 'w')
    file_str2 = json.dumps(db_results)
    file2.write(file_str2)
    file2.close()
    #  {"log3" = {"current_user": "log3", ", "subject": "subject_name","result": "number of points"},
    #  "log1" = {"current_user": "log1, ", "subject": "running","result": "8"}}

    print('\n [Results saved to the databased]')


def see_results():
    print('\n Test results...work in progress')
    # todo add function displaying results (use find function from HW16n)


# code for specific tests

def running_test():

    test_name = 'Running test'
    print(test_name)
    # todo add several multiple questions

    q1 = str(input("1. Do people run using:"
                   "\n\tA - legs"
                   "\n\tB - arms"
                   "\n\tC - people don't run"
                   "\n>>> ")).capitalize()

    q2 = str(input("\n2. Average comfortable running speed is:"
                   "\n\tA - 5km/h"
                   "\n\tB - 10km/h"
                   "\n\tC - 60km/h"
                   "\n>>> ")).capitalize()

    q3 = str(input("\n1. running ____ calories:"
                   "\n\tA - burn"
                   "\n\tB - earn"
                   "\n\tC - learn"
                   "\n>>> ")).capitalize()

    given_answers = [q1, q2, q3]
    correct_answers = ['A', 'B', 'A']
    count = 0

    # checking results
    score = 0
    for i in range(3):
        if given_answers[i] == correct_answers[i]:
            score += 4
            count += 1

        else:
            score += 0
            count += 0

    # saving results in small dictionary
    test_result = {"current user": ext_user(),
                   "subject": test_name,
                   "result": score}

    # todo add test_result to temporary db_result
    login = test_result["current user"]
    db_results[login] = test_result

    print(f"\n\t\tTest results:"
          f"\nYour answers - {given_answers}"
          f"\nCorrect answers {correct_answers}"
          f"\nNumber of correct answers - {count}"
          f"\nYou scored - {score}")

    save_results()
    go_on()


def bb_test():
    print('Bodybuilding test')
    # todo add several multiple questions
    # todo add list with correct answers
    # todo add list with answers
    # todo add check of answers given vs correct answers


def js_test():
    print('Java Script test')
    # todo add several multiple questions
    # todo add list with correct answers
    # todo add list with answers
    # todo add check of answers given vs correct answers
    save_results()


def python_test():
    print('Python test')
    # todo add several multiple questions
    # todo add list with correct answers
    # todo add list with answers
    # todo add check of answers given vs correct answers
    save_results()

