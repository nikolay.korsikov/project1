# code describing exit option
import sys


def exit_prog():
    sys.exit()


def go_on():
    print('\nDo you want to continue? [Y/N]')
    selection = str(input('>>>')).lower()
    if selection == 'n':
        exit_prog()
    else:
        pass